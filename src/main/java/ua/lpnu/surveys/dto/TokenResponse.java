package ua.lpnu.surveys.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import ua.lpnu.surveys.model.Role;

@JsonNaming(value = PropertyNamingStrategies.SnakeCaseStrategy.class)
public record TokenResponse(String token, Role role) {
}
