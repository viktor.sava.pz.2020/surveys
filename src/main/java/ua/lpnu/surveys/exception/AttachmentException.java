package ua.lpnu.surveys.exception;

public class AttachmentException extends RuntimeException {
    public AttachmentException(String message) {
        super(message);
    }
}
