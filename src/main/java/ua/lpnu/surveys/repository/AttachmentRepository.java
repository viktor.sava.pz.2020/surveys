package ua.lpnu.surveys.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.lpnu.surveys.model.Attachment;
import ua.lpnu.surveys.model.Survey;
import ua.lpnu.surveys.model.User;

import java.util.List;

@Repository
public interface AttachmentRepository extends JpaRepository<Attachment, Long> {
    List<Attachment> findAllByUser(User user);

    void deleteByUserAndSurvey(User user, Survey survey);
}
