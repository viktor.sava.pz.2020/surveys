package ua.lpnu.surveys.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.lpnu.surveys.model.Survey;

public interface SurveyRepository extends JpaRepository<Survey, Long> {
}
