package ua.lpnu.surveys.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.lpnu.surveys.model.Result;
import ua.lpnu.surveys.model.User;

import java.util.List;

public interface ResultRepository extends JpaRepository<Result, Long> {
    boolean existsBySurveyIdAndUser(Long id, User user);

    List<Result> findAllByUser(User user);

    List<Result> findAllBySurveyIdAndUser(Long surveyId, User user);
}
