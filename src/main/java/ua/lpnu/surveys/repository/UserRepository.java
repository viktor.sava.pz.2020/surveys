package ua.lpnu.surveys.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.lpnu.surveys.model.Role;
import ua.lpnu.surveys.model.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByToken(String token);

    Optional<User> findByEmail(String email);

    List<User> findAllByRole(Role role);
}
