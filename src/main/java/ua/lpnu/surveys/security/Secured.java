package ua.lpnu.surveys.security;

import ua.lpnu.surveys.model.Role;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Secured {
    Role[] roles() default {};
    boolean requiredToken() default true;
    String userParameter() default "user";
}
