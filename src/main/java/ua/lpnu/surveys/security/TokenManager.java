package ua.lpnu.surveys.security;

public interface TokenManager {

    void verify(String token);

    String generate(String subject);
}
