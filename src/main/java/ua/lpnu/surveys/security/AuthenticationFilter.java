package ua.lpnu.surveys.security;

import com.auth0.jwt.exceptions.JWTVerificationException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ua.lpnu.surveys.model.User;
import ua.lpnu.surveys.service.UserService;

import java.util.Arrays;

@Component
@Aspect
@RequiredArgsConstructor
public class AuthenticationFilter {

    private final UserService userService;
    private final TokenManager tokenManager;

    @SneakyThrows
    @Around("@annotation(secured)")
    public Object check(ProceedingJoinPoint pjp, Secured secured) {
        if (secured.requiredToken()) {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
            String authenticationHeader = request.getHeader("Authorization");
            if (authenticationHeader == null || !authenticationHeader.startsWith("Bearer ")) {
                throw new JWTVerificationException("Missing or invalid Authorization header");
            }
            String token = authenticationHeader.substring(7);
            tokenManager.verify(token);
            User user = userService.getByToken(token);
            boolean isAllowed = Arrays.stream(secured.roles())
                    .anyMatch((role) -> role.equals(user.getRole()));
            if (!isAllowed) {
                throw new JWTVerificationException("No permission exception");
            }
            Object[] args = pjp.getArgs();
            MethodSignature methodSignature = (MethodSignature) pjp.getStaticPart().getSignature();
            String[] parameterNames = methodSignature.getParameterNames();
            String userParameter = methodSignature.getMethod()
                    .getAnnotation(Secured.class)
                    .userParameter();
            if (Arrays.asList(parameterNames)
                    .contains(userParameter)) {
                int i = 0;
                for (; !parameterNames[i].equals(userParameter); i++) ;
                args[i] = user;
            }

            return pjp.proceed(args);
        }
        return pjp.proceed();
    }
}
