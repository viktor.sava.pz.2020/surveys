package ua.lpnu.surveys.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Component
public class JsonWebTokenManager implements TokenManager {

    private static final String ISSUER = "Surveys";
    private final Algorithm algorithm = Algorithm.HMAC512("secret".getBytes());
    private final JWTVerifier verifier = JWT.require(algorithm)
            .withIssuer(ISSUER)
            .build();

    @Override
    public void verify(String token) {
        verifier.verify(token);
    }

    @Override
    public String generate(String subject) {
        Instant now = Instant.now();
        return JWT.create()
                .withIssuedAt(Date.from(now))
                .withExpiresAt(Date.from(now.plus(1, ChronoUnit.DAYS)))
                .withIssuer(ISSUER)
                .withSubject(subject)
                .sign(algorithm);
    }
}
