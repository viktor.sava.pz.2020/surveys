package ua.lpnu.surveys.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.lpnu.surveys.model.Attachment;
import ua.lpnu.surveys.model.Survey;
import ua.lpnu.surveys.model.User;
import ua.lpnu.surveys.repository.AttachmentRepository;
import ua.lpnu.surveys.service.AttachmentService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AttachmentServiceImpl implements AttachmentService {
    private final AttachmentRepository repository;

    @Override
    @Transactional
    public void attach(User user, Survey survey) {
        Attachment attachment = Attachment.builder()
                .user(user)
                .survey(survey)
                .build();
        repository.save(attachment);
    }

    @Override
    @Transactional
    public void detach(User user, Survey survey) {
        repository.deleteByUserAndSurvey(user, survey);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Survey> getAvailableSurveys(User user) {
        List<Attachment> attachments = repository.findAllByUser(user);
        return attachments.stream()
                .map(Attachment::getSurvey)
                .toList();
    }
}
