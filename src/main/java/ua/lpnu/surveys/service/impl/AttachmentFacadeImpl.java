package ua.lpnu.surveys.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.lpnu.surveys.exception.AttachmentException;
import ua.lpnu.surveys.model.Survey;
import ua.lpnu.surveys.model.User;
import ua.lpnu.surveys.service.AttachmentFacade;
import ua.lpnu.surveys.service.AttachmentService;
import ua.lpnu.surveys.service.SurveyService;
import ua.lpnu.surveys.service.UserService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AttachmentFacadeImpl implements AttachmentFacade {
    private final AttachmentService attachmentService;
    private final UserService userService;
    private final SurveyService surveyService;

    @Override
    public void attach(Long userId, Long surveyId) {
        Survey survey = surveyService.get(surveyId);
        if (!survey.isAccessible()) throw new AttachmentException("Survey is not accessible");
        attachmentService.attach(userService.get(userId), survey);
    }

    @Override
    public void detach(Long userId, Long surveyId) {
        attachmentService.detach(userService.get(userId), surveyService.get(surveyId));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Survey> get(Long userId) {
        User user = userService.get(userId);
        List<Survey> attachedSurveys = attachmentService.getAvailableSurveys(user)
                .stream()
                .toList();
        return surveyService.get()
                .stream()
                .filter(p -> isNotExists(attachedSurveys, p.getId()))
                .filter(Survey::isAccessible)
                .toList();
    }

    private static boolean isNotExists(List<Survey> attachedSurveys, Long p) {
        return attachedSurveys.stream()
                .noneMatch(pl -> pl.getId()
                        .equals(p));
    }
}
