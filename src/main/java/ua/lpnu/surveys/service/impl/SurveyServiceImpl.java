package ua.lpnu.surveys.service.impl;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.lpnu.surveys.model.Role;
import ua.lpnu.surveys.model.Survey;
import ua.lpnu.surveys.model.User;
import ua.lpnu.surveys.repository.SurveyRepository;
import ua.lpnu.surveys.service.AttachmentService;
import ua.lpnu.surveys.service.SurveyService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SurveyServiceImpl implements SurveyService {
    private final AttachmentService attachmentService;
    private final SurveyRepository repository;
    @Override
    @Transactional(readOnly = true)
    public Survey get(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Survey with id %s is not found".formatted(id)));
    }

    @Override
    public List<Survey> get() {
        return repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Survey> get(User user) {
        Role role = user.getRole();
        if (role.equals(Role.PATIENT)) {
            return attachmentService.getAvailableSurveys(user);
        } else if (role.equals(Role.DOCTOR)) {
            return repository.findAll();
        }
        return List.of();
    }

    @Override
    @Transactional
    public Survey create(Survey survey, User user) {
        survey.setUser(user);
        return repository.save(survey);
    }

    @Override
    @Transactional
    public Survey update(Long id, Survey survey) {
        Survey foundSurvey = repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Survey with id %s is not found".formatted(id)));
        foundSurvey.setName(survey.getName());
        foundSurvey.setDescription(survey.getDescription());
        foundSurvey.setValues(survey.getValues());
        foundSurvey.setAccessible(survey.isAccessible());
        return repository.save(foundSurvey);
    }

    @Override
    @Transactional
    public boolean delete(Long id) {
        boolean present = repository.findById(id)
                .isPresent();
        if (present) {
            repository.deleteById(id);
        }
        return present;
    }
}
