package ua.lpnu.surveys.service.impl;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.lpnu.surveys.exception.ResultException;
import ua.lpnu.surveys.model.ProgressStatus;
import ua.lpnu.surveys.model.Result;
import ua.lpnu.surveys.model.Survey;
import ua.lpnu.surveys.model.User;
import ua.lpnu.surveys.repository.ResultRepository;
import ua.lpnu.surveys.service.AttachmentService;
import ua.lpnu.surveys.service.ResultService;
import ua.lpnu.surveys.service.SurveyService;

import java.sql.Date;
import java.time.Instant;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ResultServiceImpl implements ResultService {

    private final ResultRepository resultRepository;
    private final SurveyService surveyService;
    private final AttachmentService attachmentService;

    @Override
    @Transactional
    public Result start(Long surveyId, User user) {
        if (resultRepository.existsBySurveyIdAndUser(surveyId, user)) {
            throw new RuntimeException("Survey with id %s is already taken".formatted(surveyId));
        }
        Result result = Result.builder()
                .user(user)
                .survey(surveyService.get(surveyId))
                .date(Date.from(Instant.now()))
                .status(ProgressStatus.START)
                .build();
        return resultRepository.save(result);
    }

//    @Override
//    @Transactional
//    public Result update(Result result, User user) {
//        Result resultFromRepository = resultRepository.findById(result.getId())
//                .orElseThrow(() -> new EntityNotFoundException("Result with id %s is not found".formatted(result.getId())));
//        return resultRepository.save(Result.builder()
//                .user(resultFromRepository.getUser())
//                .survey(resultFromRepository.getSurvey())
//                .values(result.getValues())
//                .date(Date.from(Instant.now()))
//                .status(ProgressStatus.UPDATE)
//                .build());
//    }

    @Override
    @Transactional
    public void cancel(Long surveyId, User user) {
        List<Result> results = resultRepository.findAllBySurveyIdAndUser(surveyId, user);
        if (results.stream().anyMatch(p -> p.getStatus().equals(ProgressStatus.END))) {
            throw new ResultException("Survey is passed");
        }

        resultRepository.deleteAll(results);
    }

    @Override
    @Transactional
    public Result send(Long surveyId, Result result, User user) {
        Survey survey = surveyService.get(surveyId);
        attachmentService.detach(user, survey);
        return resultRepository.save(Result.builder()
                .user(user)
                .survey(survey)
                .values(result.getValues())
                .date(Date.from(Instant.now()))
                .status(ProgressStatus.END)
                .build());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Result> get(User user) {
        return resultRepository.findAllByUser(user)
                .stream()
                .filter(p -> p.getStatus()
                        .equals(ProgressStatus.END))
                .toList();
    }
}
