package ua.lpnu.surveys.service.impl;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.auth0.jwt.exceptions.JWTVerificationException;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.lpnu.surveys.dto.TokenResponse;
import ua.lpnu.surveys.model.Role;
import ua.lpnu.surveys.model.User;
import ua.lpnu.surveys.repository.UserRepository;
import ua.lpnu.surveys.security.AuthenticationException;
import ua.lpnu.surveys.security.TokenManager;
import ua.lpnu.surveys.service.UserService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final TokenManager tokenManager;

    @Override
    @Transactional
    public TokenResponse login(String email, String password) {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new AuthenticationException("User with email %s is not found".formatted(email)));
        boolean matched = BCrypt.verifyer()
                .verify(password.toCharArray(), user.getPassword()).verified;
        if (!matched) {
            throw new AuthenticationException("'password' is not correct");
        }
        String token = tokenManager.generate(user.getEmail());
        user.setToken(token);
        userRepository.save(user);
        return new TokenResponse(token, user.getRole());
    }

    @Override
    @Transactional
    public void register(User user) {
        user.setRole(Role.PATIENT);
        String rawPassword = user.getPassword();
        user.setPassword(BCrypt.withDefaults()
                .hashToString(12, rawPassword.toCharArray()));
        userRepository.save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public User getByToken(String token) {
        return userRepository.findByToken(token)
                .orElseThrow(() -> new JWTVerificationException("Token is not found"));
    }

    @Override
    @Transactional(readOnly = true)
    public User get(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("User with id %s is not found".formatted(id)));
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> getPatients() {
        return userRepository.findAllByRole(Role.PATIENT);
    }
}
