package ua.lpnu.surveys.service;

import ua.lpnu.surveys.model.Survey;

import java.util.List;

public interface AttachmentFacade {
    void attach(Long userId, Long surveyId);

    void detach(Long userId, Long surveyId);

    List<Survey> get(Long userId);
}
