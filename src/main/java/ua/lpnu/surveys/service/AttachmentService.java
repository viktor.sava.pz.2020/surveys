package ua.lpnu.surveys.service;

import ua.lpnu.surveys.model.Survey;
import ua.lpnu.surveys.model.User;

import java.util.List;

public interface AttachmentService {
    void attach(User user, Survey survey);

    void detach(User user, Survey survey);

    List<Survey> getAvailableSurveys(User user);
}
