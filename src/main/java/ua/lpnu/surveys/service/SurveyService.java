package ua.lpnu.surveys.service;

import ua.lpnu.surveys.model.Survey;
import ua.lpnu.surveys.model.User;

import java.util.List;

public interface SurveyService {
    Survey get(Long id);

    List<Survey> get();

    List<Survey> get(User user);

    Survey create(Survey survey, User user);

    Survey update(Long id, Survey survey);

    boolean delete(Long id);
}
