package ua.lpnu.surveys.service;

import ua.lpnu.surveys.model.Result;
import ua.lpnu.surveys.model.User;

import java.util.List;

public interface ResultService {
    Result start(Long surveyId, User user);

//    Result update(Result result, User user);

    Result send(Long surveyId, Result result, User user);

    void cancel(Long id, User user);

    List<Result> get(User user);
}
