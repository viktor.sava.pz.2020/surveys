package ua.lpnu.surveys.service;

import ua.lpnu.surveys.dto.TokenResponse;
import ua.lpnu.surveys.model.User;

import java.util.List;

public interface UserService {
    TokenResponse login(String email, String password);

    void register(User user);

    User getByToken(String token);

    User get(Long id);

    List<User> getPatients();
}
