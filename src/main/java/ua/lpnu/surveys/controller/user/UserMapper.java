package ua.lpnu.surveys.controller.user;

import org.mapstruct.Mapper;
import ua.lpnu.surveys.model.User;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserDto fromModel(User user);
    User toModel(UserDto userDto);
}
