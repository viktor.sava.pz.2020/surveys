package ua.lpnu.surveys.controller.user;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.bind.annotation.*;
import ua.lpnu.surveys.dto.TokenResponse;
import ua.lpnu.surveys.model.User;
import ua.lpnu.surveys.security.Secured;
import ua.lpnu.surveys.service.UserService;

import java.util.List;

@RestController
@RequestMapping("users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;

    @JsonNaming(value = PropertyNamingStrategies.SnakeCaseStrategy.class)
    record LoginRequest(
            @NotNull(message = "'email' must be provided") @Email(message = "'email' is invalid") String email,
            @NotNull(message = "'password' must be provided") String password) {
    }

    @JsonNaming(value = PropertyNamingStrategies.SnakeCaseStrategy.class)
    record RegisterRequest(
            @NotNull(message = "'first_name' must be provided") @Length(min = 3, message = "'first_name' length > 3") String firstName,
            @NotNull(message = "'last_name' must be provided") @Length(min = 3, message = "'last_name' length > 3") String lastName,
            @NotNull(message = "'email' must be provided") @Email(message = "'email' is invalid") String email,
            @NotNull(message = "'password' must be provided") @Length(min = 8, max = 24, message = "'password' length > 8 && < 24") String password) {
        public User toModel() {
            return User.builder()
                    .firstName(firstName)
                    .lastName(lastName)
                    .email(email)
                    .password(password)
                    .build();
        }
    }

    @PostMapping("login")
    @Secured(requiredToken = false)
    public TokenResponse login(@RequestBody @Valid LoginRequest loginRequest) {
        return userService.login(loginRequest.email(), loginRequest.password());
    }

    @PostMapping("register")
    @Secured(requiredToken = false)
    public void register(@RequestBody @Valid RegisterRequest registerRequest) {
        userService.register(registerRequest.toModel());
    }

    @GetMapping("patients")
    public List<UserDto> getPatients() {
        return userService.getPatients()
                .stream()
                .map(userMapper::fromModel)
                .toList();
    }

}
