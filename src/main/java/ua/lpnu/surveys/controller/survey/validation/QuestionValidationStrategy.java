package ua.lpnu.surveys.controller.survey.validation;

import ua.lpnu.surveys.controller.survey.QuestionDto;

public interface QuestionValidationStrategy {
    boolean validate(QuestionDto questionDto);
    String getType();
}
