package ua.lpnu.surveys.controller.survey;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ua.lpnu.surveys.model.Role;
import ua.lpnu.surveys.model.User;
import ua.lpnu.surveys.security.Secured;
import ua.lpnu.surveys.service.SurveyService;

import java.util.List;

@RestController
@RequestMapping("surveys")
@RequiredArgsConstructor
public class SurveyController {

    private final SurveyService surveyService;
    private final SurveyMapper surveyMapper;

    @GetMapping
    @Secured(roles = {Role.DOCTOR, Role.PATIENT})
    @Operation(description = """
            Returns all surveys (doctor role);
            Returns all available surveys (that were attached by doctor for patient)""")
    public List<SurveyDto> get(@Parameter(hidden = true) User user) {
        return surveyService.get(user)
                .stream()
                .map(surveyMapper::fromModel)
                .toList();
    }

    @GetMapping("{id}")
    @Secured(roles = {Role.PATIENT, Role.DOCTOR})
    @Operation(description = """
            Returns info about survey
            """)
    public SurveyDto get(@PathVariable Long id, @Parameter(hidden = true) User user) {
        SurveyDto surveyDto = surveyMapper.fromModel(surveyService.get(id));
        Role role = user.getRole();
        if (role.equals(Role.PATIENT)) {
            surveyDto.setAccessible(null);
        }
        return surveyDto;
    }

    @PostMapping
    @Secured(roles = {Role.DOCTOR})
    @Operation(description = "Creates new survey")
    public SurveyDto create(@RequestBody @Valid SurveyDto surveyDto, @Parameter(hidden = true) User user) {
        return surveyMapper.fromModel(surveyService.create(surveyMapper.toModel(surveyDto), user));
    }

    @PutMapping("{id}")
    @Secured(roles = {Role.DOCTOR})
    @Operation(description = "Updates survey")
    public SurveyDto update(@PathVariable Long id, @RequestBody @Valid SurveyDto surveyDto) {
        return surveyMapper.fromModel(surveyService.update(id, surveyMapper.toModel(surveyDto)));
    }

    @DeleteMapping("{id}")
    @Secured(roles = Role.DOCTOR)
    @Operation(description = "Deletes survey")
    public void delete(@PathVariable Long id) {
        surveyService.delete(id);
    }

}
