package ua.lpnu.surveys.controller.survey;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonNaming(value = PropertyNamingStrategies.SnakeCaseStrategy.class)
public class QuestionDto {
    @NotBlank(message = "'type' must be provided")
    private String type;
    @NotBlank(message = "'question' must be provided")
    private String question;
    private List<String> options;
}
