package ua.lpnu.surveys.controller.survey;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import ua.lpnu.surveys.model.Survey;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class SurveyMapper {
    @Autowired
    protected ObjectMapper objectMapper;
    protected final TypeReference<List<QuestionDto>> ref = new TypeReference<>() {
    };

    @Mapping(target = "values", expression = "java(objectMapper.writeValueAsString(surveyDto.getQuestions()))")
    protected abstract Survey toModelImpl(SurveyDto surveyDto) throws JsonProcessingException;

    @SneakyThrows
    public Survey toModel(SurveyDto surveyDto) {
        return toModelImpl(surveyDto);
    }

    @Mapping(target = "questions", expression = "java(objectMapper.readValue(survey.getValues(), ref))")
    protected abstract SurveyDto fromModelImpl(Survey survey) throws JsonProcessingException;

    @SneakyThrows
    public SurveyDto fromModel(Survey survey) {
        return fromModelImpl(survey);
    }
}
