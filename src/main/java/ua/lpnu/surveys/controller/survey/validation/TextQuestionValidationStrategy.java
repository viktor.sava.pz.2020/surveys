package ua.lpnu.surveys.controller.survey.validation;

import org.springframework.stereotype.Component;
import ua.lpnu.surveys.controller.survey.QuestionDto;

@Component
public class TextQuestionValidationStrategy implements QuestionValidationStrategy {
    @Override
    public boolean validate(QuestionDto questionDto) {
        return questionDto.getOptions() == null || questionDto.getOptions().size() == 0;
    }

    @Override
    public String getType() {
        return "text";
    }
}
