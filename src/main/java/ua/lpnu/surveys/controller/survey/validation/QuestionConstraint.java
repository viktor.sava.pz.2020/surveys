package ua.lpnu.surveys.controller.survey.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = QuestionValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface QuestionConstraint {
    String message() default "Invalid question construct";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
