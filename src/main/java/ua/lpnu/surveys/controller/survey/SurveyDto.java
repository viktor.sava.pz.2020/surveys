package ua.lpnu.surveys.controller.survey;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import ua.lpnu.surveys.controller.survey.validation.QuestionConstraint;

import java.util.List;

@Getter
@Setter
@JsonNaming(value = PropertyNamingStrategies.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SurveyDto {
    private Long id;
    private Boolean accessible;
    @NotBlank(message = "'name' must be provided")
    private String name;
    @NotBlank(message = "'description' must be provided")
    private String description;
    @QuestionConstraint(message = "'questions' must be in right format")
    private List<QuestionDto> questions;
}
