package ua.lpnu.surveys.controller.survey.validation;

import org.springframework.stereotype.Component;
import ua.lpnu.surveys.controller.survey.QuestionDto;

@Component
public class MultiQuestionValidationStrategy implements QuestionValidationStrategy {
    @Override
    public boolean validate(QuestionDto questionDto) {
        return questionDto.getOptions().size() >= 2;
    }

    @Override
    public String getType() {
        return "multi";
    }
}
