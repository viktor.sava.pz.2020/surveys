package ua.lpnu.surveys.controller.survey.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.lpnu.surveys.controller.survey.QuestionDto;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class QuestionValidator implements ConstraintValidator<QuestionConstraint, List<QuestionDto>> {
    private final Map<String, QuestionValidationStrategy> questionValidationStrategy;

    @Autowired
    public QuestionValidator(List<QuestionValidationStrategy> questionValidationStrategy) {
        this.questionValidationStrategy = questionValidationStrategy.stream()
                .collect(Collectors.toMap(QuestionValidationStrategy::getType, v -> v));
    }

    @Override
    public boolean isValid(List<QuestionDto> questions, ConstraintValidatorContext context) {
        return questions.stream()
                .allMatch(value -> getStrategy(value).validate(value));
    }

    private QuestionValidationStrategy getStrategy(QuestionDto value) {
        return questionValidationStrategy.get(value.getType());
    }
}
