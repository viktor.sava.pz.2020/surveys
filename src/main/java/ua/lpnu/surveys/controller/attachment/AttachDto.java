package ua.lpnu.surveys.controller.attachment;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonNaming(value = PropertyNamingStrategies.SnakeCaseStrategy.class)
public class AttachDto {
    @NotNull(message = "'user_id' must be provided")
    private Long userId;
    @NotNull(message = "'survey_id' must be provided")
    private Long surveyId;
}
