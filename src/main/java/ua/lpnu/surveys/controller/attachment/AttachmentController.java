package ua.lpnu.surveys.controller.attachment;

import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ua.lpnu.surveys.controller.survey.SurveyDto;
import ua.lpnu.surveys.controller.survey.SurveyMapper;
import ua.lpnu.surveys.model.Role;
import ua.lpnu.surveys.security.Secured;
import ua.lpnu.surveys.service.AttachmentFacade;

import java.util.List;

@RestController
@RequestMapping("attachments")
@RequiredArgsConstructor
public class AttachmentController {
    private final AttachmentFacade attachmentFacade;
    private final SurveyMapper surveyMapper;

    @GetMapping("{userId}")
    @Secured(roles = Role.DOCTOR)
    @Operation(description = "Returns not yet attached surveys by user id")
    public List<SurveyDto> get(@PathVariable Long userId) {
        return attachmentFacade.get(userId)
                .stream()
                .map(surveyMapper::fromModel)
                .toList();
    }

    @PostMapping
    @Secured(roles = Role.DOCTOR)
    @Operation(description = "Attaches survey for user")
    public void attach(@RequestBody @Valid AttachDto attachDto) {
        attachmentFacade.attach(attachDto.getUserId(), attachDto.getSurveyId());
    }

    @DeleteMapping
    @Secured(roles = Role.DOCTOR)
    @Operation(description = "Detaches survey for user")
    public void detach(@RequestBody @Valid AttachDto attachDto) {
        attachmentFacade.detach(attachDto.getUserId(), attachDto.getSurveyId());
    }

}
