package ua.lpnu.surveys.controller.result;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import ua.lpnu.surveys.model.ProgressStatus;

import java.util.List;

@Getter
@Setter
@JsonNaming(value = PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ResultDto {
    private Long id;

    private List<AnswerDto> answers;

    private ProgressStatus status;
}
