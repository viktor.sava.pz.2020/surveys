package ua.lpnu.surveys.controller.result;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonNaming(value = PropertyNamingStrategies.SnakeCaseStrategy.class)
public class AnswerDto {
    @NotBlank(message = "'type' must be provided [text, single, multi]")
    private String type;
    @NotBlank(message = "'question' must be provided")
    private String question;
    @NotNull(message = "'answer' must be provided")
    private List<String> answer;
}
