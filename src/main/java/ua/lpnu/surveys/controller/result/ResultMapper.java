package ua.lpnu.surveys.controller.result;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import ua.lpnu.surveys.model.Result;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class ResultMapper {
    @Autowired
    protected ObjectMapper objectMapper;
    protected final TypeReference<List<AnswerDto>> ref = new TypeReference<>() {
    };

    @Mapping(target = "values", expression = "java(objectMapper.writeValueAsString(resultDto.getAnswers()))")
    protected abstract Result toModelImpl(ResultDto resultDto) throws JsonProcessingException;

    @SneakyThrows
    public Result toModel(ResultDto resultDto) {
        return toModelImpl(resultDto);
    }

    @Mapping(target = "answers", expression = "java(result.getValues() != null ? objectMapper.readValue(result.getValues(), ref) : java.util.List.of())")
    protected abstract ResultDto fromModelImpl(Result result) throws JsonProcessingException;

    @SneakyThrows
    public ResultDto fromModel(Result result) {
        return fromModelImpl(result);
    }

}
