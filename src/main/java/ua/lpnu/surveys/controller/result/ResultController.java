package ua.lpnu.surveys.controller.result;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ua.lpnu.surveys.model.Result;
import ua.lpnu.surveys.model.Role;
import ua.lpnu.surveys.model.User;
import ua.lpnu.surveys.security.Secured;
import ua.lpnu.surveys.service.ResultService;

import java.util.List;

@RestController
@RequestMapping("results")
@RequiredArgsConstructor
public class ResultController {

    private final ResultService resultService;
    private final ResultMapper resultMapper;

    @GetMapping
    @Secured(roles = Role.PATIENT)
    @Operation(description = "Returns list of passed surveys (already completed)")
    public List<ResultDto> get(@Parameter(hidden = true) User user) {
        return resultService.get(user)
                .stream()
                .map(resultMapper::fromModel)
                .toList();
    }

    @GetMapping("{id}")
    @Secured(roles = Role.PATIENT)
    @Operation(description = "Starts passing surveys")
    public ResultDto start(@PathVariable("id") Long surveyId, @Parameter(hidden = true) User user) {
        Result result = resultService.start(surveyId, user);
        return resultMapper.fromModel(result);
    }

//    @PutMapping
//    @Secured(roles = Role.PATIENT)
//    @Operation(description = "Don't use it")
//    public ResultDto update(@RequestBody @Valid ResultDto resultDto, @Parameter(hidden = true) User user) {
//        return resultMapper.fromModel(resultService.update(resultMapper.toModel(resultDto), user));
//    }

    @PostMapping("{id}")
    @Secured(roles = Role.PATIENT)
    @Operation(description = """
            Changes status of passing survey, automatically detaches survey for user.
            Fill result dto with question-answer format, look at AnswerDto""")
    public ResultDto send(@PathVariable("id") Long surveyId, @RequestBody @Valid ResultDto resultDto, @Parameter(hidden = true) User user) {
        return resultMapper.fromModel(resultService.send(surveyId, resultMapper.toModel(resultDto), user));
    }


    @DeleteMapping("{id}")
    @Secured(roles = Role.PATIENT)
    @Operation(description = """
            Cancels passing survey
            """)
    public void delete(@PathVariable Long id, User user) {
        resultService.cancel(id, user);
    }
}
