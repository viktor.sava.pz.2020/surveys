package ua.lpnu.surveys.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "attachments")
@NoArgsConstructor
@AllArgsConstructor
public class Attachment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(cascade = CascadeType.REMOVE, optional = false)
    @JoinColumn(name = "survey_id")
    private Survey survey;

    public static AttachmentBuilder builder() {
        return new AttachmentBuilder();
    }

    public static final class AttachmentBuilder {
        private Long id;
        private User user;

        private Survey survey;

        private AttachmentBuilder() {
        }

        public AttachmentBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public AttachmentBuilder user(User user) {
            this.user = user;
            return this;
        }

        public AttachmentBuilder survey(Survey survey) {
            this.survey = survey;
            return this;
        }

        public Attachment build() {
            Attachment attachment = new Attachment();
            attachment.setId(id);
            attachment.setUser(user);
            attachment.setSurvey(survey);
            return attachment;
        }
    }
}
