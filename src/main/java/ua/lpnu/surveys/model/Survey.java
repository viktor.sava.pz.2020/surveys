package ua.lpnu.surveys.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

@Getter
@Setter
@Entity
@Table(name = "surveys")
@NoArgsConstructor
@AllArgsConstructor
public class Survey {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false, length = 1024)
    private String description;

    @Lob
    private String values;

    @ColumnDefault("false")
    private boolean accessible;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public static SurveyBuilder builder() {
        return new SurveyBuilder();
    }

    public static final class SurveyBuilder {
        private Long id;
        private String name;
        private String description;
        private String values;
        private boolean accessible;
        private User user;

        private SurveyBuilder() {
        }

        public SurveyBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public SurveyBuilder name(String name) {
            this.name = name;
            return this;
        }

        public SurveyBuilder description(String description) {
            this.description = description;
            return this;
        }

        public SurveyBuilder values(String values) {
            this.values = values;
            return this;
        }

        public SurveyBuilder accessible(boolean accessible) {
            this.accessible = accessible;
            return this;
        }

        public SurveyBuilder user(User user) {
            this.user = user;
            return this;
        }

        public Survey build() {
            Survey survey = new Survey();
            survey.setId(id);
            survey.setName(name);
            survey.setDescription(description);
            survey.setValues(values);
            survey.setAccessible(accessible);
            survey.setUser(user);
            return survey;
        }
    }
}