package ua.lpnu.surveys.model;

public enum Role {
    PATIENT, DOCTOR
}
