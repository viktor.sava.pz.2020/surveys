package ua.lpnu.surveys.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "results")
@NoArgsConstructor
@AllArgsConstructor
public class Result {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, optional = false)
    @JoinColumn(name = "survey_id", nullable = false)
    private Survey survey;

    private Date date;

    @Enumerated(EnumType.STRING)
    private ProgressStatus status;

    @Lob
    private String values;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public static ResultBuilder builder() {
        return new ResultBuilder();
    }

    public static final class ResultBuilder {
        private Long id;
        private Survey survey;
        private Date date;
        private ProgressStatus status;
        private String values;
        private User user;

        private ResultBuilder() {
        }

        public ResultBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public ResultBuilder survey(Survey survey) {
            this.survey = survey;
            return this;
        }

        public ResultBuilder date(Date date) {
            this.date = date;
            return this;
        }

        public ResultBuilder status(ProgressStatus status) {
            this.status = status;
            return this;
        }

        public ResultBuilder values(String values) {
            this.values = values;
            return this;
        }

        public ResultBuilder user(User user) {
            this.user = user;
            return this;
        }

        public Result build() {
            Result result = new Result();
            result.setId(id);
            result.setSurvey(survey);
            result.setDate(date);
            result.setStatus(status);
            result.setValues(values);
            result.setUser(user);
            return result;
        }
    }
}