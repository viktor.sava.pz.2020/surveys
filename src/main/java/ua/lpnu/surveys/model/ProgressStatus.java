package ua.lpnu.surveys.model;

public enum ProgressStatus {
    START, UPDATE, END
}
